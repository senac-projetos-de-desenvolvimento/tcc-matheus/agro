export default {
    fontFamily: 'Lato',
    colors: {
        today: '#2965',
        text: '#000',
        secondary: '#FFF',
        mainText: '#222',
        subText: '#555'
    }
}