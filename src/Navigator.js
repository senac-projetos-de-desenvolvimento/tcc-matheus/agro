import React from 'react'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createDrawerNavigator } from 'react-navigation-drawer'

import Auth from './screens/Auth'
import Home from './screens/Home'
import Func from './screens/Func'
import Dash from './screens/Dashboard'
import Pluviometer from './screens/Pluviometer'
import Maintenance from './screens/Maintenance'
import Menu from './screens/Menu'
import commonStyles from './commonStyles'


const menuConfig = {
    initialRouteName: 'Dashboard',
    contentComponent: Menu,
    contentOptions: {
        labelStyle: {
            fontFamily: commonStyles.fontFamily,
            fontWeight: 'normal',
            fontSize: 20
        },
        activeLabelStyle: {
            color: '#080',
            fontWeight: 'bold',
        }
    }
}

const menuRoutes = {
    Dashboard: {
        name: 'Dashboard',
        screen: props => <Dash title='Inicio' daysAhead={0} {...props} />,
        navigationOptions: {
            title: 'Inicio'
        }
    },


    Deliverie: {
        name: 'Deliverie',
        screen: props => <Home title='Controle Diesel' daysAhead={0} {...props} />,
        navigationOptions: {
            title: 'Controle Diesel'
        }
    },

    Func: {
        name: 'Func',
        screen: props => <Func title='Funcionários' daysAhead={0} {...props} />,
        navigationOptions: {
            title: 'Funcionários'
        }
    },

    Maintenance: {
        name: 'Maintenance',
        screen: props => <Maintenance title='Manutenções' daysAhead={0} {...props} />,
        navigationOptions: {
            title: 'Manutenções dos Implementos'
        }
    },

    Pluviometer: {
        name: 'Pluviometer',
        screen: props => <Pluviometer title='Pluviômetro' daysAhead={0} {...props} />,
        navigationOptions: {
            title: 'Pluviômetro'
        }
    }
}


const menuNavigator = createDrawerNavigator(menuRoutes, menuConfig)

const mainRoutes = {
    Auth: {
        name: 'Auth',
        screen: Auth
    },
    Home: {
        name: 'Home',
        screen: menuNavigator
    },
    Func: {
        name: 'Func',
        screen: Func
    },
    Dash: {
        name: 'Dash',
        screen: Dash
    },
    Pluviometer: {
        name: 'Pluviometer',
        screen: Pluviometer
    },
    Maintenance: {
        name: 'Maintenance',
        screen: Maintenance
    },
}

const mainNavigator = createSwitchNavigator(mainRoutes, {
    initialRouteName: 'Auth'
})
export default createAppContainer(mainNavigator)