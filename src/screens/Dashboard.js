import React, { Component } from 'react'
import { View, Text, StatusBar, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements'
import { DrawerActions } from 'react-navigation-drawer';


import AsyncStorage from '@react-native-community/async-storage'

const colors = {
    themeColor: "#4263ec",
    white: "#fff",
    background: "#f4f6fc",
    greyish: "#a4a4a4",
    tint: "#2b49c3"
}


export default class Dashboard extends Component {


    constructor(props) {
        super(props)
        this.state = {
            user: "await this.getUserData()"
        }
    }

    componentDidMount() {
        this.getUserData()
    }

    async getUserData() {
        const stateString = await AsyncStorage.getItem('userData')
        let a = JSON.parse(stateString)
        this.setState({ user: a.name })
    }

    // loadUser = async () => {
    //     try {
    //         const res = await axios.get(`http://192.168.111.1:3000/user/name`)
    //         this.setState({ deliveries: res.data }, this.filterDeliveries)
    //     } catch (e) {
    //         showError(e)
    //     }
    // }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.themeColor }}>
                <StatusBar barStyle="light-content" backgroundColor={colors.themeColor}></StatusBar>
                <View style={{ backgroundColor: colors.themeColor }}>
                    <View style={{ padding: 16, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity>
                            <Icon raised name='reorder' type='material' size={30} style={{ color: colors.white }}
                                onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())} />
                        </TouchableOpacity>
                        {/* <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity>
                                <Icon raised name='user' type='evilicon' size={30} style={{ color: colors.white }}></Icon>
                            </TouchableOpacity>
                        </View> */}
                    </View>
                    <View style={{ padding: 16 }}>
                        <Text style={{ color: colors.white, fontSize: 30 }}>Bem-vindo {"\n"}{this.state.user}</Text>
                        <View style={{ paddingHorizontal: 16, paddingVertical: 6, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: colors.tint, borderRadius: 20, marginVertical: 20, alignItems: 'center' }}>
                            <Icon name='search' type='evilicon' size={30} style={{ color: colors.white }} />
                        </View>
                    </View>
                </View>
                <View style={{ padding: 20, flexDirection: 'row', backgroudColor: colors.white, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 24, color: colors.white, justifyContent: 'center' }}>Menu</Text>
                </View>
                <View style={{ backgroundColor: colors.white, flexDirection: 'row', marginHorizontal: 16, marginVertical: 4, borderRadius: 20, paddingVertical: 20, paddingHorizontal: 20 }}>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Func")}>
                        <Icon name='people' type='material' size={40} style={{ color: colors.white }} ></Icon>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Pluviometer")} >
                        <Icon name='cloud' type='material' size={40} style={{ marginLeft: 90, color: colors.white }}></Icon>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Maintenance")} >
                        <Icon name='build' type='material' size={40} style={{ marginLeft: 100, color: colors.white }}></Icon>
                    </TouchableOpacity>
                </View>
                <View style={{ marginTop: 30, backgroundColor: colors.white, flexDirection: 'row', marginHorizontal: 16, marginVertical: 4, borderRadius: 20, paddingVertical: 20, paddingHorizontal: 20 }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Deliverie")} >
                        <Icon name='train' type='material' size={40} style={{ marginLeft: 70, color: colors.white }}></Icon>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => Actions.inbox()} >
                        <Icon name='beenhere' type='material' size={40} style={{ marginLeft: 100, color: colors.white }}></Icon>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}