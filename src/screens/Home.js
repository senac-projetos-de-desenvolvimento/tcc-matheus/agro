import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    Platform
} from 'react-native'

import tomorrow from '../../assets/imgs/tomorrow.jpg'

import { Icon } from 'react-native-elements'
import Icons from 'react-native-vector-icons/FontAwesome'

import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { server, showError } from '../common'

import moment from 'moment'
import 'moment/locale/pt-br'
import commonStyles from '../commonStyles.js'
import Object from '../components/Object'
import AddDeliverie from './AddDeliverie'

const initialState = {
    showDoneDeliveries: true,
    showAddDeliverie: false,
    visibleDeliveries: [],
    deliveries: []
}

const colors = {
    themeColor: "#4263ec",
    white: "#fff",
    background: "#f4f6fc",
    greyish: "#a4a4a4",
    tint: "#2b49c3"
}

export default class Home extends Component {

    state = {
        ...initialState
    }

    componentDidMount = async () => {
        const stateString = await AsyncStorage.getItem('deliveriesState')
        const savedState = JSON.parse(stateString) || initialState
        this.setState({
            showDoneTasks: savedState.showDoneDeliveries
        }, this.filterDeliveries)

        this.loadDeliveries()
        console.disableYellowBox = true
    }
    _hideModal = () => {
        console.error()
        this.loadDeliveries()
    }


    loadDeliveries = async () => {
        try {
            const res = await axios.get(`http://192.168.111.1:3000/deliveries`)
            this.setState({ deliveries: res.data }, this.filterDeliveries)
        } catch (e) {
            showError(e)
        }

    }

    toggleFilter = () => {
        this.setState({ showDoneDeliveries: !this.state.showDoneDeliveries })
    }
    syncButton = () => {
        this.loadDeliveries()
    }

    filterDeliveries = () => {
        let visibleDeliveries = null
        if (this.state.showDoneDeliveries) {
            visibleDeliveries = [...this.state.deliveries]
        } else {
            const pending = deliverie => deliverie.doneAt === null
            visibleDeliveries = this.state.deliveries.filter(pending)
        }

        this.setState({ visibleDeliveries })
        AsyncStorage.setItem('deliverieState'), JSON.stringify({
            showDoneDeliveries: this.state.showDoneDeliveries
        })
    }


    toggleDeliverie = async deliverieId => {
        try {
            await axios.put(`${server}/deliveries/${deliverieId}/toggle`)
            this.loadDeliveries()
        } catch (e) {
            showError(e)
        }
    }

    deleteDeliveries = async deliveriesId => {
        try {
            await axios.delete(`${server}/deliveries/${deliveriesId}`)
            this.loadDeliveries()
        } catch (e) {
            showError(e)
        }
    }

    render() {
        const today = moment().locale('pt-br').format('ddd, D [de] MMMM')
        return (
            <View style={styles.container}>
                <AddDeliverie isVisible={this.state.showAddDeliverie}
                    onCancel={() => this.setState({ showAddDeliverie: false })}
                    parentReference={() => this.loadDeliveries()}
                />
                <ImageBackground source={tomorrow} style={styles.background}>
                    <TouchableOpacity  >
                        <Icon raised name='reorder' type='material' size={20} style={{ color: colors.white }}
                            onPress={() => this.props.navigation.openDrawer()}></Icon>
                    </TouchableOpacity>
                    <View style={styles.titleBar}>
                        <Text style={styles.title}>Controle Diesel</Text>
                        <Text style={styles.subtitle}>{today}</Text>
                    </View>
                </ImageBackground>
                {/* <View style={styles.iconBar}>
                    <TouchableOpacity onPress={this.toggleFilter}>
                        <Icons name={this.state.showDoneDeliveries ? 'eye' : 'eye-slash'} size={20} color={commonStyles.colors.primary} />
                    </TouchableOpacity>
                </View> */}
                <View style={styles.home}>
                    <FlatList data={this.state.visibleDeliveries} keyExtractor={item => `${item.id}`}
                        renderItem={({ item }) => <Object {...item}
                            onToggleObject={this.toggleDeliverie} onDelete={this.deleteDeliveries} />} />
                </View>
                <TouchableOpacity style={styles.addButton}
                    activeOpacity={0.7}
                    onPress={() => this.setState({ showAddDeliverie: true })}>
                    <Icons name="plus" size={20} color={commonStyles.colors.today} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    background: {
        flex: 3
    },
    home: {
        flex: 7
    },
    titleBar: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    title: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 50,
        color: commonStyles.colors.secondary,
        marginLeft: 20,
        marginBottom: 20,
        fontWeight: "bold"
    },
    subtitle: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 20,
        color: commonStyles.colors.secondary,
        marginLeft: 20,
        marginBottom: 30,
        fontWeight: "bold"
    },
    iconBar: {
        flexDirection: 'row',
        marginHorizontal: 20,
        justifyContent: 'flex-end',
        marginTop: 15
    },
    button3: {
        marginLeft: 15
    },
    addButton: {
        position: 'absolute',
        right: 30,
        bottom: 30,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: commonStyles.colors.today,
        justifyContent: 'center',
        alignItems: 'center'
    }
})