
import React, { Component } from 'react'
import {
    View,
    ActivityIndicator,
    StyleSheet
} from 'react-native'

import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

export default class AuthOrApp extends Component {

    componentDidMount = async () => {
        const getUserDataJson = await AsyncStorage.getItem('getUserData')
        let getUserData = null

        try {
            getUserData = JSON.parse(getUserDataJson)
        } catch (e) {
        }

        if (getUserData && getUserData.token) {
            axios.defaults.headers.common['Authorization'] = `bearer ${getUserData.token}`
            this.props.navigation.navigate('Dash', getUserData)
        } else {
            this.props.navigation.navigate('Auth')
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator size='large' />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000'
    }
})